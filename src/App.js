import React, { Component } from 'react';
import Creatable from './components/creatable';
import './App.css';

const generateTestOptions = counts => {
  const options = [];

  for (let x=1; x <= counts; x++) {
    options.push({ label: `option ${x}`, value: x });
  }
  
  return options;
}

class App extends Component {
  constructor() {
    super();

    this.state = {
      value: undefined,
    };
  }

  render() {
    return (
      <div className="App">
        <Creatable
          options={generateTestOptions(10)}
          value={this.state.value}
          onSelect={value => this.setState({ value })}
          placeholder='select a value'
        />
      </div>
    );
  }
}

export default App;
