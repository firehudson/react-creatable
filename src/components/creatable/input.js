import React from 'react';
import omit from './helpers/omit';
import './index.css';

const Input = props => (
  <input
    className='input'
    type='text'
    ref={props.assignRef}
    {...omit(props, 'assignRef')}
  />
);

export default Input;
