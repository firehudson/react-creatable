import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import OptionItem from './option-item';
import './index.css';

class OptionList extends Component {
  componentDidUpdate(prevProps) {
    if (this.props.highlightedOption !== prevProps.highlightedOption) {
      this.ensureActiveItemVisible();
    }
  }

  ensureActiveItemVisible() {
    var itemComponent = this.refs.activeItem;
    if (itemComponent) {
      var domNode = ReactDOM.findDOMNode(itemComponent);
      domNode.scrollIntoView(!this.props.movingDown);
    }
  }

  render() {
    const { options, onSelect, highlightedOption } = this.props;

    return (
      <div className='list-group'>
        {
          options.map((option, index) =>
            <OptionItem
              key={index}
              onSelect={onSelect} {...option}
              active={index === highlightedOption}
              ref={index === highlightedOption ? 'activeItem' : `nonActiveItem${index}`}
            />
          )
        }
      </div>
    )
  }
}

export default OptionList;
