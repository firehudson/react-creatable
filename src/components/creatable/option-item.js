import React, { Component } from 'react';
import './index.css';

class OptionItem extends Component {
  render() {
    const { label, value, onSelect, isNewOption, active } = this.props;

    return (
      <div className={`list-group-item ${active ? 'active' : ''}`} onClick={() => onSelect(value, isNewOption)}>
        <p className='list-group-item-text'>
          {label}
        </p>
      </div>
    );
  }

}

export default OptionItem;
