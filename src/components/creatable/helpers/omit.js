export default (object, keys) => {
  const target = {};

  Object.keys(object).forEach((key) => {
    if (!keys.includes(key))
      target[key] = object[key];
  });

  return target;
}
