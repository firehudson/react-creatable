import React, { Component } from 'react';
import OptionList from './options-list';
import Input from './input';
import './index.css';

const ENTER_KEY = 13;
const DOWN_KEY = 40;
const UP_KEY = 38;

class Creatable extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isFocused: false,
      value: '',
      createdOptions: [],
      highlightedOption: undefined,
      movingDown: undefined,
    };
  }

  getOptions = () => [
    ...this.state.createdOptions,
    ...this.props.options,
  ];

  invokeParentOnSelect = (value) => {
    this.props.onSelect(value);
  }

  generateNewOption = (value) => {
    return [
      ...this.state.createdOptions,
      {
        value,
        label: value,
      },
    ]
  }

  handleOnSelect = (value, isNewOption=false) => {
    this.setState({
      isFocused: false,
      value: '',
      createdOptions: isNewOption ? this.generateNewOption(value) : this.state.createdOptions,
    }, () => this.invokeParentOnSelect(value));
  }

  getSelectedOptionLabel = () => {
    const { value } = this.props;
    let label;

    this.getOptions().forEach(option => {
      if (option.value === value) {
        label = option.label
      }
    });

    return label;
  }

  getFilteredOptions = () => {
    const { value } = this.state;

    if (value === '')
      return this.getOptions();

    const filteredOptions = this.getOptions()
      .filter(({ label }) => label.includes(value));

    if (!filteredOptions.length)
      return [{
        value,
        isNewOption: true,
        label: `Create option "${value}"`,
      }];

    return filteredOptions;
  }

  handleKeyUp = ({ keyCode }) => {
    const { value, highlightedOption } = this.state;
    const totalOptions = this.getOptions().length;

    switch (keyCode) {
      case ENTER_KEY:
        value.length && this.handleOnSelect(value, true);
        this.blurInputField();
      break;

      case DOWN_KEY:
        if (highlightedOption === totalOptions - 1) {
          break;
        }

        this.setState({
          highlightedOption: highlightedOption + 1 || 0,
          movingDown: true,
        });
      break;

      case UP_KEY:
        if (highlightedOption === 0) {
          break;
        }

        this.setState({
          highlightedOption: highlightedOption - 1 || 0,
          movingDown: false,
        });
      break;

      default:
      break;
    }
  }

  blurInputField = () => this.input.blur();

  render() {
    const { isFocused, value, highlightedOption, movingDown } = this.state;
    const { placeholder } = this.props;

    return (
      <div className='container' onBlur={this.blurInputField}>
        <Input
          assignRef={(input) => { this.input = input; }}
          onFocus={() => this.setState({ isFocused: true })}
          value={value}
          onChange={(e) => this.setState({ value: e.target.value })}
          placeholder={this.getSelectedOptionLabel() || placeholder}
          onKeyUp={this.handleKeyUp}
        />

        {
          isFocused && <OptionList
            movingDown={movingDown}
            highlightedOption={highlightedOption}
            onSelect={this.handleOnSelect}
            options={this.getFilteredOptions()}
          />
        }
      </div>
    );
  }
}

Creatable.defaultProps = {
  options: undefined,
  onSelect: () => undefined,
  value: undefined,
  placeholder: undefined,
};

export default Creatable;
